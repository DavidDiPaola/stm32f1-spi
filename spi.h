/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#ifndef _SPI_H
#define _SPI_H

enum spi_frame {
	spi_frame_8bit,
	spi_frame_16bit,
};

enum spi_order {
	spi_order_msbfirst,
	spi_order_lsbfirst,
};

enum spi_clkspd {
	spi_clkspd_div2,
	spi_clkspd_div4,
	spi_clkspd_div8,
	spi_clkspd_div16,
	spi_clkspd_div32,
	spi_clkspd_div64,
	spi_clkspd_div128,
	spi_clkspd_div256,
};

enum spi_mode {
	spi_mode_slave,
	spi_mode_master,
};

enum spi_clkpol {
	spi_clkpol_idle0,
	spi_clkpol_idle1,
};

enum spi_clkpha {
	spi_clkpha_first,
	spi_clkpha_second,
};

void
spi_init_SPI1_MASTER_GPIOA(
	enum spi_frame  frame,
	enum spi_order  order,
	enum spi_clkspd clkspd,
	enum spi_clkpol clkpol,
	enum spi_clkpha clkpha
);

void
spi_write(struct spi * SPI, u16 data);

#endif

