# stm32f1-spi
STM32F1 SPI library

[GitLab](https://gitlab.com/DavidDiPaola/stm32f1-spi)
[GitHub](https://github.com/DavidDiPaola/stm32f1-spi)

to be used with [stm32f1-common](https://gitlab.com/DavidDiPaola/stm32f1-common)

