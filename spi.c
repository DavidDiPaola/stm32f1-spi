/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <io/rcc.h>
#include <io/gpio.h>
#include <io/afio.h>
#include <io/spi.h>

#include "spi.h"

static void
_init(
	struct spi * SPI,
	enum spi_frame  frame,
	enum spi_order  order,
	enum spi_clkspd clkspd,
	enum spi_mode   mode,
	enum spi_clkpol clkpol,
	enum spi_clkpha clkpha
) {
	while ((SPI->SR & spi_SR_BSY) != 0) {}
	SPI->CR1 &= ~(spi_CR1_SPE);

	u32 cr1 = SPI->CR1;

	cr1 &= ~(spi_CR1_BIDIMODE);

	switch (frame) {
	case spi_frame_8bit:
		cr1 &= ~(spi_CR1_DFF);
		break;
	case spi_frame_16bit:
		cr1 |= spi_CR1_DFF;
		break;
	}

	switch (order) {
	case spi_order_msbfirst:
		cr1 &= ~(spi_CR1_LSBFIRST);
		break;
	case spi_order_lsbfirst:
		cr1 |= spi_CR1_LSBFIRST;
		break;
	}

	cr1 &= ~(spi_CR1_BR_MASK);
	switch (clkspd) {
	case spi_clkspd_div2:
		cr1 |= spi_CR1_BR_DIV2;
		break;
	case spi_clkspd_div4:
		cr1 |= spi_CR1_BR_DIV4;
		break;
	case spi_clkspd_div8:
		cr1 |= spi_CR1_BR_DIV8;
		break;
	case spi_clkspd_div16:
		cr1 |= spi_CR1_BR_DIV16;
		break;
	case spi_clkspd_div32:
		cr1 |= spi_CR1_BR_DIV32;
		break;
	case spi_clkspd_div64:
		cr1 |= spi_CR1_BR_DIV64;
		break;
	case spi_clkspd_div128:
		cr1 |= spi_CR1_BR_DIV128;
		break;
	case spi_clkspd_div256:
		cr1 |= spi_CR1_BR_DIV256;
		break;
	}

	switch (mode) {
	case spi_mode_slave:
		cr1 &= ~(spi_CR1_MSTR);
		break;
	case spi_mode_master:
		cr1 |= spi_CR1_MSTR;
		break;
	}

	switch (clkpol) {
	case spi_clkpol_idle0:
		cr1 &= ~(spi_CR1_CPOL);
		break;
	case spi_clkpol_idle1:
		cr1 |= spi_CR1_CPOL;
		break;
	}

	switch (clkpha) {
	case spi_clkpha_first:
		cr1 &= ~(spi_CR1_CPHA);
		break;
	case spi_clkpha_second:
		cr1 |= spi_CR1_CPHA;
		break;
	}

	cr1 |= spi_CR1_SPE;

	SPI->CR1 = cr1;
}

void
spi_init_SPI1_GPIOA(
	enum spi_frame  frame,
	enum spi_order  order,
	enum spi_clkspd clkspd,
	enum spi_mode   mode,
	enum spi_clkpol clkpol,
	enum spi_clkpha clkpha
) {
	/* RM0008 figure 1, figure 8, figure 11 */
	RCC->APB2ENR |= (rcc_APB2ENR_SPI1EN | rcc_APB2ENR_IOPAEN | rcc_APB2ENR_AFIOEN);
	AFIO->MAPR = ((AFIO->MAPR & afio_MAPR_SPI1REMAP_MASK) | afio_MAPR_SPI1REMAP_0);

	/* RM0008 table 56, table 25 */
	u32 crl = GPIOA->CRL;
	u32 odr = GPIOA->ODR;
	if (mode == spi_mode_slave) {
		/* TODO SPI1 GPIOA slave mode */
	}
	else if (mode == spi_mode_master) {
		/* MOSI pin (PA7) */
		crl = (crl & ~(gpio_CRL_CNF7_MASK )) | gpio_CRL_CNF7_OUT_AFPP;
		crl = (crl & ~(gpio_CRL_MODE7_MASK)) | gpio_CRL_MODE7_OUT_50MHZ;
		/* MISO pin (PA6) */
		crl = (crl & ~(gpio_CRL_CNF6_MASK )) | gpio_CRL_CNF6_IN_PUPD;
		crl = (crl & ~(gpio_CRL_MODE6_MASK)) | gpio_CRL_MODE6_IN;
		odr |= (1 << 10);
		/* SCK pin (PA5) */
		crl = (crl & ~(gpio_CRL_CNF5_MASK )) | gpio_CRL_CNF5_OUT_AFPP;
		crl = (crl & ~(gpio_CRL_MODE5_MASK)) | gpio_CRL_MODE5_OUT_50MHZ;
	}
	/* done */
	GPIOA->CRL = crl;
	GPIOA->ODR = odr;

	_init(SPI1,
		frame,
		order,
		clkspd,
		mode,
		clkpol,
		clkpha
	);
}

void
spi_write(struct spi * SPI, u16 data) {
	while ((SPI->SR & spi_SR_BSY) != 0) {}
	SPI->DR = data;
}

